const { Client } = require('@elastic/elasticsearch');

const client = new Client({
  node: 'http://localhost:9200',
});

const data = async () => {
  // await client.indices.delete({ index: 'data' });
  await client.indices.refresh({ index: 'data' });
  await client.index({
    index: 'data',
    body: {
      id: '1',
      title: 'having fun with elasticsomething',
      descritpion: 'Stack the data',
      amount: '1',
    },
  });
  await client.index({
    index: 'data',
    body: {
      id: '2',
      title: ' Is prisma same same ? ',
      descritpion: 'forget about prisma 2',
      amount: '2',
    },
  });
  await client.index({
    index: 'data',
    body: {
      id: '3',
      title: 'when do i get paid ? ',
      descritpion: 'you are still an intern',
      amount: null,
    },
  });
};

module.exports = data;
