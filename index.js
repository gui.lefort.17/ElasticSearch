const { Client } = require('@elastic/elasticsearch');

const client = new Client({
  node: 'http://localhost:9200',
});

const run = async () => {
  await client.indices.refresh({ index: 'data' });
  const { body } = client.search(
    {
      index: 'data',
      method: 'get',
      body: {
        query: {
          match: { id: '1' },
        },
      },
    },
    (error, result) => {
      console.log(' 🚀 request : ', result.meta.request);
      if (error) console.log(' ⛔️ error : ', error);
      if (result.body.hits) {
        console.log(' 🍺 result : ', result?.body.hits.hits);
      } else {
        console.log(' 🌱 result : ', result);
      }
    }
  );
};

run().catch(console.log);
